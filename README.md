# Profiles REST API

Profiles REST API course code.

*Install via sudo:*
- python3-dev
- libpq-dev
- libxml2-dev

*What do we have in this project?*
- Rest framework API with filter and authentication
- API Login and authentication
- Creation of user profiles
- A feed with updating status